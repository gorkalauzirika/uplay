// This function is automatically called by the player once it loads
function onYouTubePlayerReady(playerId) {
  player = document.getElementById(playerId) 
  player.addEventListener("onStateChange", "onPlayerStateChange");
  Players.youtube.setPlayer(player);
  playerController.setPlayerReady(playerId);
}

function onPlayerStateChange(newState)
{
	 playerController.playerStateChanged("youtube",newState);
}
