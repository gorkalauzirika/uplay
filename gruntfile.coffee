module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON "package.json"

    meta:
      endpoint: ''
      banner: """
        /* <%= pkg.name %> v<%= pkg.version %> - <%= grunt.template.today("m/d/yyyy") %>
           <%= pkg.homepage %>
           Copyright (c) <%= grunt.template.today("yyyy") %> <%= pkg.author.name %> - Licensed <%= _.pluck(pkg.license, "type").join(", ") %> */

        """

    source:
      coffee: 
        player:
          [ 'source/controller/player/*.coffee',
            'source/model/*.coffee',
            'source/view/player/*.coffee',
            'source/player.coffee']

        server:
          [  'source/controller/server/*.coffee',
             'source/server.coffee' ]


        remote:
          [ 'source/controller/remote/*.coffee',
            'source/view/remote/*.coffee',
            'source/view/remote/providerViews/*.coffee',
            'source/remote.coffee']

        index:
          [ 'source/index.coffee']


      stylesheets: 'source/stylesheets/'

      jade: 'source/jade/'

    coffee:
      compile: 
        files: 
          '<%= meta.endpoint %>js/player.debug.js': ['<%= source.coffee.player %>']
          '<%= meta.endpoint %>server.debug.js': ['<%= source.coffee.server %>']
          '<%= meta.endpoint %>js/remote.debug.js': ['<%= source.coffee.remote %>']
          '<%= meta.endpoint %>js/index.debug.js': ['<%= source.coffee.index %>']

    jade:
      release:
        files:
          'player.html': '<%= source.jade %>player.jade'
          'dictatorship.html': '<%= source.jade %>dictatorship.jade'
          'index.html': '<%= source.jade %>index.jade'

    uglify:
      options: compress: false, banner: "<%= meta.banner %>"
      coffee: 
        files: 
          '<%= meta.endpoint %>js/index.js': '<%= meta.endpoint %>js/index.debug.js'
          '<%= meta.endpoint %>server.js': '<%= meta.endpoint %>server.debug.js'
          '<%= meta.endpoint %>js/remote.js': '<%= meta.endpoint %>js/remote.debug.js'
          '<%= meta.endpoint %>js/player.js': '<%= meta.endpoint %>js/player.debug.js'

    compass:
      dist: 
        options: 
          sassDir: "<%= source.stylesheets %>"
          cssDir:'<%= meta.endpoint %>css'     

    watch:
      coffee:
        files: ["<%= source.coffee.index %>", "<%= source.coffee.server %>", '<%= source.coffee.remote %>' , '<%= source.coffee.player %>']
        tasks: ["coffee"]
      stylesheets:
        files: ["<%= source.stylesheets %>*.scss"]
        tasks: ["compass"]
      jade:
        files: ["<%= source.jade %>*.jade"]
        tasks: ["jade"]


  grunt.loadNpmTasks "grunt-contrib-coffee"
  grunt.loadNpmTasks "grunt-contrib-uglify"
  grunt.loadNpmTasks "grunt-contrib-compass"
  #grunt.loadNpmTasks "grunt-contrib-concat"
  grunt.loadNpmTasks 'grunt-contrib-jade'
  grunt.loadNpmTasks "grunt-contrib-watch"


  grunt.registerTask "default", ["coffee", "uglify", "compass","jade"] #"concat"]

  ###
    concat:
      css:
        src: ['<%= source.css_core %>'],
        dest: '<%= meta.endpoint %>static/stylesheets/watch.css'
###
