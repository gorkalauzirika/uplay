$(document).ready ->
	window.Views = window.Views || {}

	Views.article = new ArticleView()
	Views.header = new HeaderView()	
	Views.footer = new FooterView()
	Views.volume = new VolumeView()

	window.Controller = new RemoteController()

	#Hide address bar
	setTimeout -> 
		window.scrollTo 0, 1
	, 100


