express = require 'express'
http = require 'http'
app = do express
server = app.listen process.env.PORT || 8080
io = require('socket.io').listen(server, { log: false })

app.configure ->
  app.set 'views',  __dirname + "/source/jade"
  app.set 'view engine', 'jade' 
  app.use "/css", express.static(__dirname + '/css') 
  app.use "/js", express.static(__dirname + '/js')  
  app.use "/images", express.static(__dirname + '/images')
  app.use express.bodyParser()

controllers = {}

app.get '/', (req, res) ->
  res.sendfile __dirname + '/index.html' 

app.get '/login', (req, res) ->
  res.render "login.jade"

app.post '/login', (req, res) ->
  room = req.body.roomName
  pass = req.body.password
  if controllers[room]?
    if controllers[room].isPasswordCorrect pass
      res.cookie 'sessionId' , controllers[room].getSessionId(), { maxAge: 7200000, httpOnly: true }
      if req.body.type? and req.body.type is "tv"
        res.redirect "/player/#{room}"
      else
        res.redirect "/#{room}"
    else
      res.render "login.jade", {error: "Contraseña incorrecta"}
  else
      res.render "login.jade", {error: "No existe la sala"}

app.post '/new', (req, res) -> 
  room = req.body.roomName
  pass = req.body.password
  if controllers[room]?
    res.render "login.jade", {error: "La sala ya existe"}
  else
    controllers[room] = new exports.ServerController(io, room, pass) 
    res.cookie 'sessionId' , controllers[room].getSessionId(), { maxAge: 7200000, httpOnly: true }
    res.redirect "/player/#{room}"

app.get '/player/:room', (req, res) -> 
  room = req.params.room
  if controllers[room]?
    res.sendfile __dirname + '/player.html' 
  else
    res.render "login.jade", {error: "La sala #{room} no existe"}

app.get '/:room', (req, res) -> 
  room = req.params.room
  if controllers[room]?
    res.sendfile(__dirname + '/dictatorship.html') 
  else
    res.render "login.jade", {error: "La sala #{room} no existe"}