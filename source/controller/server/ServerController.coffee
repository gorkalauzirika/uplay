crypto = require('crypto')

class ServerController
	constructor: (io,room, password) ->
		@io = io
		@playlist = []
		@currentMedia = -1
		@room = room
		@createdAt = new Date()
		@password = password
		@sessionId =  crypto.randomBytes(20).toString('hex')
		
		ctrl = @

		io.of("/#{@room}").authorization (handshakeData, callback) ->
			#Parse cookies
			cookies = handshakeData.headers.cookie?.split /[;,] */
			cookies = cookies or [] 
			sessionId = ""
			for cookie in cookies 
				if cookie.substring(0,9) is "sessionId"
					sessionId = cookie.split("=")[1]

			if sessionId is ctrl.sessionId
				callback(null,true)
			else
				callback(null,false)
		.on 'connection',  (socket) ->

			socket.emit 'playlistUpdated', {playlist: ctrl.playlist, position: ctrl.currentMedia}

			socket.on 'videoFinished', (data) ->
				do ctrl.nextMedia

			socket.on 'playMedia', ->
				do ctrl.playMedia

			socket.on 'pauseMedia', ->
				do ctrl.pauseMedia

			socket.on 'nextMedia', ->
				do ctrl.nextMedia

			socket.on 'goToMedia', (media) ->
				ctrl.goToMedia media

			socket.on 'addMedia', (media)->
				ctrl.addToPlaylist (media)

			socket.on 'removeMedia', (media)->
				ctrl.removeFromPlaylist (media)

			socket.on 'currentTimeChanged', (time) ->
				ctrl.io.of("/#{ctrl.room}").emit 'timeUpdated', time

			socket.on 'seekTo', (time) ->
				ctrl.io.of("/#{ctrl.room}").emit 'seekTo', time

			socket.on 'volumeChanged', (volume) ->
				ctrl.io.of("/#{ctrl.room}").emit 'volumeChanged', volume

	isPasswordCorrect: (password) ->
		if @password is password then true else false
	
	getSessionId: ->
		@sessionId

	addToPlaylist: (media) ->
		@playlist.push media
		@io.of("/#{@room}").emit 'playlistUpdated',{playlist: @playlist, position: @currentMedia}

	removeFromPlaylist: (media) ->
		index = parseInt media.index
		#Validate the index given
		if index >= 0 and index < @playlist.length
			@playlist.splice index, 1
			#Correct the actual media position if the deleted one was before in the list
			if @currentMedia > index
				@currentMedia--
			else if @currentMedia is index and index >= @playlist.length #if is the last and current stop playing all
				do @stopMedia
				@currentMedia = -1
			else if @currentMedia is index #if deleted one was the same that was playing and it isnt the last one, go to next
				if @playlist[@currentMedia]?
					@io.of("/#{@room}").emit 'nextMedia', {media: @playlist[@currentMedia], position: @currentMedia}		
			
			@io.of("/#{@room}").emit 'playlistUpdated', {playlist: @playlist, position: @currentMedia}

	playMedia: ->
		@io.of("/#{@room}").emit('playMedia')

	pauseMedia: ->
		@io.of("/#{@room}").emit('pauseMedia')

	stopMedia: ->
		@io.of("/#{@room}").emit('stopMedia')

	nextMedia: ->
		if @playlist.length > @currentMedia + 1 
			@currentMedia++
			if @playlist[@currentMedia]?
				@io.of("/#{@room}").emit 'nextMedia',  {media: @playlist[@currentMedia], position: @currentMedia}

	goToMedia: (media) ->
		index = parseInt media.index
		#Validate the index given
		if index >= 0 and index < @playlist.length
			@currentMedia = index
			@io.of("/#{@room}").emit 'nextMedia',  {media: @playlist[@currentMedia], position: @currentMedia}


exports.ServerController = ServerController