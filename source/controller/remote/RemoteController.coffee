class RemoteController
	constructor: ->
		@socket = io.connect "#{window.location.pathname}"

		@socket.of("#{window.location.pathname}")
			.on 'connect_failed',  (reason) ->
				window.location.replace "/login"

		@socket.on 'playlistUpdated', (playlist) ->
			Views.article.updatePlaylist playlist.playlist
			Views.article.setCurrentMedia playlist.position
			Views.footer.showNewMedia playlist.playlist[playlist.position]
			Views.footer.setDuration playlist.playlist[playlist.position]?.duration
					
		@socket.on 'playMedia', ->
			do Views.footer.showPauseButton

		@socket.on 'pauseMedia', ->
			do Views.footer.showPlayButton

		@socket.on 'nextMedia', (media) ->
			Views.footer.showNewMedia media.media
			Views.article.setCurrentMedia media.position
			Views.footer.setCurrentTime 0
			Views.footer.setDuration media.media.duration
			do Views.footer.showPauseButton

		@socket.on 'stopMedia', ->
			do Views.footer.hideMedia
			do Views.footer.showPlayButton

		@socket.on 'timeUpdated', (time) ->
			Views.footer.setCurrentTime time

		@socket.on 'volumeChanged', (volume) ->
			Views.volume.setCurrentVolume volume

	validateConnection: ->
		ctrl = @
		$.post document.URL + '/validate', (resp) ->
			ctrl.socket.emit 'joinRoom', resp

	emitPlayMedia: ->
		@socket.emit 'playMedia'

	emitPauseMedia: ->
		@socket.emit 'pauseMedia'
			
	emitNextMedia: (media)->
		@socket.emit 'nextMedia' , media

	emitGoToMedia: (media) ->
		@socket.emit 'goToMedia' , media

	emitAddMedia: (media) ->
		@socket.emit 'addMedia' , media

	emitRemoveMedia: (media) ->
		@socket.emit 'removeMedia' , media

	emitSeekTo: (time) ->
		@socket.emit 'seekTo' , time

	emitVolumeChange: (volume) ->
		@socket.emit 'volumeChanged', volume


window.RemoteController = RemoteController