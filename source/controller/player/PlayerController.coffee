class PlayerController
	constructor: ->
		Players.youtube = new YoutubeView()
		Players.soundcloud = new SoundcloudView() 
		@currentPlayer = null
		ctrl = @
		window.onresize = (ev) ->
			do ctrl.currentPlayer.goFullScreen

	setPlayerReady: (playerId) ->
		do @initMedia

	initMedia: ->
		ctrl = @

		urlArray = window.location.pathname.split('/')
		room = urlArray[urlArray.length - 1]

		@socket = io.connect "/#{room}"

		@socket.of("/#{room}")
			.on 'connect_failed',  (reason) ->
				window.location.replace "/login"

		@socket.on 'nextMedia', (media) ->
			#Iterate across all players and stop all medias to prevent to players running at the same time
			for name, player of Players
				do player.stopMedia
				do player.hidePlayer

			if media.media.type is "youtube"
				ctrl.currentPlayer = Players.youtube
				
			else if media.media.type is "soundcloud"
				ctrl.currentPlayer = Players.soundcloud
			else
				return

			do $("div.no-media").hide

			ctrl.currentPlayer.startMedia media.media
			
		@socket.on 'playMedia', (media) ->
			do ctrl.currentPlayer.playMedia

		@socket.on 'pauseMedia', (media) ->
			do ctrl.currentPlayer.pauseMedia

		@socket.on 'stopMedia', (media) ->
			do ctrl.currentPlayer.stopMedia
			#Show background
			do $("div.no-media").show

		@socket.on 'seekTo', (time) ->
			ctrl.currentPlayer?.seekTo time

		@socket.on 'volumeChanged', (volume) ->
			ctrl.currentPlayer?.changeVolume volume

		setInterval ->
			do ctrl.emitCurrentTime
		, 4000

	playerStateChanged: (playerId,newState) ->
		#check if state is finished (0)
		if newState is 0
			@socket.emit 'videoFinished', {videoId:""}

	emitCurrentTime: ->
		currentTime = do @currentPlayer?.getCurrentTime
		if currentTime?
			@socket.emit 'currentTimeChanged', currentTime

window.PlayerController = PlayerController

