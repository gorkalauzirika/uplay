class ArticleView extends Backbone.View
	el: $("article")

	events: ->
		"click .show-more-search-items": "showMoreSearchItems"

	initialize: ->
		#jQuery selectors
		@$searchList = @$el.children("#search-list")
		@$playlist = @$el.children("#play-list")

		@playlistViews = [] #Stores each view of the playlist
		
		@lastSearch = "" #Stores last seach value in order to avoid retrieving wrong items if string in search box is changed

		@currentProvider = "soundcloud"

	searchByString: (string, page = 0, provider) ->
		if provider?
			@currentProvider = provider
			
		#Hide playlist
		do @$playlist.hide
		#Show loading gif

		#Only remove previous items if is first page
		if page is 0 then @$searchList.html ""

		if string isnt ""
			@lastSearch = string
			#Fetch results
			articleV = @
			if @currentProvider is "youtube"
				$.getJSON("https://gdata.youtube.com/feeds/api/videos?q=#{string}&start-index=#{1+page*10}&max-results=10&v=2&alt=json&callback=?", {})
				.done (resp) ->
					for video in resp.feed.entry
						item = new YoutubeItemView model: video
						articleV.$searchList.append item.render().el
					if resp.feed.entry.length is 10
						articleV.$searchList.append "<div class='show-more-search-items' data-next-page='#{page + 1}'><p>Mostar más</p></div>"
			else if @currentProvider is "soundcloud"
				$.getJSON("https://api.soundcloud.com/tracks?client_id=7b776d5f8e35e0b342e5100325de15bf&q=#{string}&limit=10&offset=#{1+page*10}&filter=streamable", {})
				.done (resp) ->
					for media in resp
						if media.streamable 
							item = new SoundcloudItemView model: media
							articleV.$searchList.append item.render().el
					if resp.length is 10
						articleV.$searchList.append "<div class='show-more-search-items' data-next-page='#{page + 1}'><p>Mostar más</p></div>"

	showMoreSearchItems: ->
		page = $(".show-more-search-items").data "next-page"
		do $(".show-more-search-items").remove
		@searchByString @lastSearch, page

	showPlaylist: ->
		do @$searchList.hide
		do @$playlist.show		

	showSearchlist:->
		do @$playlist.hide 
		do @$searchList.show		

	updatePlaylist: (list) ->
		#Delete exisiting views
		for view in @playlistViews
			do view.undelegateEvents
			do $(view.el).removeData().unbind
			do view.remove
		@playlistViews = []
		#Add new views
		i = 0
		for media in list
			item = new PlaylistItemView model: media
			item.setIndex i
			if @currentMedia is i
				item.isPlaying true #Highligth the item that is playing
			i++
			@$playlist.append item.render().el
			@playlistViews.push item

	setCurrentMedia: (position) ->
		@currentMedia = position
		#Update the item that is highlighted, first set all to isPlaying -> false to remove the highlight in case
		#it exists and then highlight the current media
		for view in @playlistViews
			view.isPlaying false

		@playlistViews[@currentMedia]?.isPlaying true


window.ArticleView = ArticleView
	
