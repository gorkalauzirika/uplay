class VolumeView extends Backbone.View
	el: $("#volume-popup")

	events: ->
		"mousedown": "startTrackingMove"
		"touchstart": "startTrackingMove"
		"mousemove": "trackMove"
		"touchmove": "trackMove"
		"mouseup": "stopTrackingMove"
		"touchend": "stopTrackingMove"

	initialize: ->
		@volumeBarDragging = false
		@$volumeBar = @$el.find "#volume-progress"

	startTrackingMove: ->
		@volumeBarDragging = true

	trackMove: (e)->
		if @volumeBarDragging
			do e.preventDefault
			yPos = e.pageY || e.originalEvent.touches[0].pageY
			position = yPos -  @$volumeBar.offset().top
			percentage = 100 * position / @$volumeBar.parent().height()
			@$volumeBar.css "height","#{percentage}%" 

	stopTrackingMove: ->
		if @volumeBarDragging
			@volumeBarDragging = false
			percentage = 100 * @$volumeBar.height() / @$volumeBar.parent().height()
			Controller.emitVolumeChange 100 - percentage # The bar is upside down

	toggleView: ->
		do @$el.toggle

	setCurrentVolume: (volume) ->
		if volume > 100
			volume = 100
		else if volume < 0
			volume = 0
		@$volumeBar.css "height","#{100 - volume}%"


window.VolumeView = VolumeView