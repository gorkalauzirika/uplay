class PlaylistItemView extends Backbone.View
	tagName: "li"

	template: _.template '<div class="image-container"><img src="<%= thumbnail %>"><i class="icon-provider-<%= type %>"></i></div><div class="video-info"><h4><%= name %></h4><p><%= author %></p><div class="button-container"><span class="play-media"><i class="icon-playlist-play"></i></span><span class="remove-from-playlist"><i class="icon-playlist-remove"></i></span></div></div>'

	events: ->
		"click .remove-from-playlist": "removeFromPlaylist"
		"click .play-media": "playMedia"

	initialize: ->
		@index = -1

	render: ->
		@$el.html @template @model
		@$el.addClass "#{@model.type}-item"
		@		

	setIndex: (index) ->
		@index = index 

	removeFromPlaylist: ->
		@$el.find(".remove-from-playlist").hide()
		Controller.emitRemoveMedia  {index: @index}

	playMedia: ->
		do @$el.find(".play-media").hide
		Controller.emitGoToMedia  {index: @index}
		ctrl = @
		setTimeout ->
			do ctrl.$el.find(".play-media").show
		, 3000

	isPlaying: (playing) ->
		if playing
			@$el.addClass "playing"
		else
			@$el.removeClass "playing"

window.PlaylistItemView = PlaylistItemView