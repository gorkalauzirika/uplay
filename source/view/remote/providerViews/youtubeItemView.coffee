class YoutubeItemView extends ProviderItemView
	className: "youtube-item"

	initialize: ->
		@id = @model.media$group.yt$videoid.$t
		@name = @model.media$group.media$title.$t
		@author = @model.media$group.media$credit[0].$t
		@thumbnail = @model.media$group.media$thumbnail[1].url
		@duration = @model.media$group.yt$duration.seconds
		mins = Math.floor(@duration / 60)
		secs = @duration - mins * 60
		secs = if secs < 10 then "0#{secs}" else secs
		@durationText = "#{mins}:#{secs}"
		@type = "youtube"
		#model.yt$statistics.viewCount
		
window.YoutubeItemView = YoutubeItemView