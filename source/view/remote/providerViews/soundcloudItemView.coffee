class SoundcloudItemView extends ProviderItemView
	className: "soundcloud-item"

	initialize: ->
		@id = @model.id
		@name = @model.title
		@author = @model.user.username
		@thumbnail = @model.artwork_url || "https://a2.sndcdn.com/assets/images/default/cloudx120-1ec56ce9.png"
		@duration = Math.round(@model.duration / 1000)
		mins = Math.floor(@duration / 60)
		secs = @duration - mins * 60
		secs = if secs < 10 then "0#{secs}" else secs
		@durationText = "#{mins}:#{secs}"
		@type = "soundcloud"
		

window.SoundcloudItemView = SoundcloudItemView