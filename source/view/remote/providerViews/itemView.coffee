class ProviderItemView extends Backbone.View
	tagName: "li"

	template: _.template '<div class="image-container"><img src="<%= thumbnail %>"><span class="duration"><%=durationText%></span></div><div class="video-info"><h4><%= name %></h4><p><%= author %></p><p> views</p><div class="button-container"><span class="add-to-playlist"><i class="icon-playlist-add"></i></span><span class="successfully-added" style="display:none"><i class="icon-playlist-tick"></i></span></div></div>'

	events: ->
		"click .add-to-playlist": "addToPlaylist"

	render: ->
		@$el.html @template @
		@

	addToPlaylist: ->
		@$el.find(".add-to-playlist").hide()
		Controller.emitAddMedia {type: @type, id: @id, name: @name, author: @author, thumbnail: @thumbnail, duration: @duration}
		do @$el.find(".successfully-added").show
			

window.ProviderItemView = ProviderItemView