class YoutubeItemView extends Backbone.View
	tagName: "li"
	className: "youtube-item"

	template: _.template '<div class="image-container"><img src="<%= thumbnail %>"><span class="duration"><%=durationText%></span></div><div class="video-info"><h4><%= name %></h4><p><%= author %></p><p><%= model.yt$statistics.viewCount %> views</p><div class="button-container"><span class="add-to-playlist"><i class="icon-playlist-add"></i></span><span class="successfully-added" style="display:none"><i class="icon-playlist-tick"></i></span></div></div>'

	events: ->
		"click .add-to-playlist": "addToPlaylist"

	initialize: ->
		@id = @model.media$group.yt$videoid.$t
		@name = @model.media$group.media$title.$t
		@author = @model.media$group.media$credit[0].$t
		@thumbnail = @model.media$group.media$thumbnail[1].url
		@duration = @model.media$group.yt$duration.seconds
		mins = Math.floor(@duration / 60)
		secs = @duration - mins * 60
		secs = if secs < 10 then "0#{secs}" else secs
		@durationText = "#{mins}:#{secs}"

	render: ->
		@$el.html @template @
		@

	addToPlaylist: ->
		@$el.find(".add-to-playlist").hide()
		Controller.emitAddMedia {type:"soundcloud", id: @id, name: @name, author: @author, thumbnail: @thumbnail, duration: @duration}
		do @$el.find(".successfully-added").show
			

window.YoutubeItemView = YoutubeItemView