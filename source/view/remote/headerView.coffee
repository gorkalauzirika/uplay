class HeaderView extends Backbone.View

	el: $("header")

	MODE_PLAYLIST : 0 #Playlist is shown
	MODE_SEACHING : 1 #Searched items are shown

	events: ->
		"click button#search" : "searchItems"
		"click button#playlist" : "showPlaylist"
		"click button#back" : "goToNormalMode"
		"keyup #search-box": "autocompleteSearch"
		"click .autocomplete-item": "selectAutocomplete"
		"click .icon-remove-search": "removeSearchString"
		"click .provider-item": "changeProvider"

	initialize: ->
		@buttonSearch = @$el.children("#search")
		@buttonBack = @$el.children("#back")
		@boxSearch =  @$el.children("#search-box")
		@playlistHeader =  @$el.children("#list-header")
		@listAutocomplete = @$el.children("#autocomplete-list")
		@listProvider = @$el.children("#provider-list")

		@selectedProvider  = "youtube"

		do @goToNormalMode

	searchItems: ->
		if @mode is @MODE_PLAYLIST
			do @showSearchBox
		else if @listProvider.is ":hidden"
			do @listProvider.show
		else
			searchString = @boxSearch.children(".search-string").val()
			if searchString isnt ""
				Views.article.searchByString searchString, 0, @selectedProvider
				@listAutocomplete.html ""
			do @listProvider.hide

	autocompleteSearch: ->
		query = @boxSearch.children("input").val()
		if query isnt ""
			view = @
			$.getJSON("http://suggestqueries.google.com/complete/search?client=firefox&hl=es&ds=yt&q=#{query}&callback=?", {})
			.done (resp) ->
				view.listAutocomplete.html ""
				i = 0
				#API doesnt allow limiting the results and returns always 10 but we only want 5
				for it in resp[1]
					if i < 5
						view.listAutocomplete.append "<li class=autocomplete-item>#{it}</li>"
					i++
		else
			@listAutocomplete.html ""

	selectAutocomplete: (ev) ->
		newVal = ev.currentTarget.textContent
		@boxSearch.children("input").val newVal
		@listAutocomplete.html ""
		Views.article.searchByString newVal

	removeSearchString: ->
		@boxSearch.children("input").val ""

	changeProvider: (ev) ->
		newProv = ev.currentTarget.getAttribute "data-provider"
		@setProvider newProv
		do @listProvider.hide
		
	showPlaylist: ->
		@listAutocomplete.html ""
		do Views.article.showPlaylist
		
	showSearchBox: ->
		do @buttonBack.show
		do @boxSearch.show
		do @playlistHeader.hide
		do Views.article.showSearchlist
		@buttonSearch.children("#provider").removeClass().addClass "icon-provider-#{@selectedProvider}"
		do @buttonSearch.children("#down-arrow").show

		@mode = @MODE_SEACHING

	goToNormalMode: ->
		@mode = @MODE_PLAYLIST
		do @buttonBack.hide
		do @boxSearch.hide
		do @buttonSearch.show
		do @playlistHeader.show
		do @showPlaylist
		do @listProvider.hide
		@buttonSearch.children("#provider").removeClass().addClass "icon-search"
		do @buttonSearch.children("#down-arrow").hide

	setProvider: (provider) ->
		@selectedProvider = provider
		@buttonSearch.children("#provider").removeClass().addClass "icon-provider-#{@selectedProvider}"
		@boxSearch.children("input").attr "placeholder", "Buscar en #{provider}"
		do @searchItems
		



window.HeaderView = HeaderView
