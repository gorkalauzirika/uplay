class FooterView extends Backbone.View

	el: $("footer")

	events: ->
		"click li#play" : "playMedia"
		"click li#pause" : "pauseMedia"
		"click li#next" : "nextMedia"
		"click li#volume": "showVolumeMenu"
		"mousedown div#progress": "startTrackingProgressBar"
		"mousemove": "seekToPosition"
		"mouseup": "stopTrackingProgressBar"
		"touchstart div#progress": "startTrackingProgressBar"
		"touchmove":"seekToPosition"
		"touchend": "stopTrackingProgressBar"

	initialize: ->
		@currentTime = 0
		@duration = 0
		@playing = false
		@$progressBar = @$el.find("#progress-bar")
		@progressBarDragging = false

	playMedia: ->
		do Controller.emitPlayMedia

	pauseMedia: ->
		do Controller.emitPauseMedia
			
	nextMedia: ->
		do Controller.emitNextMedia

	#Progress bar events
	startTrackingProgressBar: ->
		@progressBarDragging = true
		@$progressBar.css "height", "20px"

	seekToPosition: (e)->
		if @progressBarDragging 
			do e.preventDefault
			xPos = e.pageX || e.originalEvent.touches[0].pageX
			position = xPos - @$progressBar.offset().left
			percentage = 100 * position / @$progressBar.parent().width()
			@$progressBar.css "width","#{percentage}%" 
			@$progressBar.css "transition-duration" : "0s"

	stopTrackingProgressBar: ->
		if @progressBarDragging
			@progressBarDragging = false
			@$progressBar.css "height", "5px"
			percentage =  @$progressBar.width() / @$progressBar.parent().width()
			Controller.emitSeekTo percentage * @duration
	#Button events
	showPlayButton: ->
		do @$el.find("#pause").hide
		do @$el.find("#play").show
		@playing = true

	showPauseButton: ->
		do @$el.find("#play").hide
		do @$el.find("#pause").show
		@playing = false

	showNewMedia: (media) ->
		if media?
			@$el.children(".media-name").children("p").text media.name
		else
			@$el.children(".media-name").children("p").text ""
	
	showVolumeMenu: ->
		do Views.volume.toggleView

	#Control methods
	hideMedia: ->
		@$el.children(".media-name").children("p").text ""


	setCurrentTime: (time)->
		if @duration is 0 or time is 0
			currentProgress = 0
			transitionSecs = "0s"
			@currentTime = 0
		else		
			if @currentTime > time - 7 
				@currentTime = time + 5 				
				transitionSecs = "5s"
			else
				@currentTime = time
				transitionSecs = "0s"		

			currentProgress = (@currentTime / @duration) * 100

			if currentProgress > 100 
		    	currentProgress = 100

		@$progressBar.css "width","#{currentProgress}%" 
		@$progressBar.css "transition-duration" : transitionSecs

	setDuration: (duration) ->
		@duration = duration
			
window.FooterView = FooterView