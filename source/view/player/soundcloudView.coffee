class SoundcloudView extends Backbone.View
	el: $("#soundcloud")
 
	initialize: ->
		@playerId = "soundcloud"
		@sound = null
		@$mediaImage = @$el.find("#media-image")
		@$mediaName = @$el.find("#media-name")

	startMedia: (media)->
		view = @
		if @sound?
			do @stopMedia

		SC.stream "/tracks/#{media.id}", (sound) ->
			view.sound = sound
			view.loadMediainfo media
			do view.playMedia
			do view.goFullScreen				

	playMedia: ->
		if @sound?
			@sound.play onfinish:-> playerController.playerStateChanged "soundcloud", 0	

	pauseMedia: ->
		if @sound?
			do @sound.pause

	stopMedia: ->
		if @sound?
			do @sound.stop

	seekTo: (time)->
		if @sound?
			@sound.setPosition time * 1000

	changeVolume: (volume)->
		if @sound?
			@sound.setVolume volume

	getCurrentTime: ->
		if @sound?
			@sound.position / 1000

	goFullScreen: ->
		do @$el.show

	hidePlayer: ->
		do @$el.hide

	loadMediainfo: (media) ->
		@$mediaImage.attr "src", media.thumbnail
		@$mediaName.text media.name


window.SoundcloudView = SoundcloudView