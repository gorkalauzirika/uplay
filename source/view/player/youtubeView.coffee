class YoutubeView extends Backbone.View
	tagName: "div"
	className: "player"
 
	initialize: ->
		@playerId = "youtube"
		@videoId = ""
		@active = false
		$("article").append @$el
		do @createPlayer

	createPlayer: ->
		$(@el).attr 'id', @playerId
		if @playerId?
			swfobject.embedSWF "http://www.youtube.com/apiplayer?version=3&enablejsapi=1&playerapiid=#{@playerId}", 
	                     @playerId, "0", "0", "9", null, {wmode:"transparent"}, { allowScriptAccess: "always"},  { id: @playerId, class:"player" }

	setPlayer: (player) ->
		@player = player
		do @hidePlayer

	startMedia: (media) ->
		videoId = media.id
		if @videoId isnt videoId
			@player.loadVideoById videoId, 0 , "hd720"
			@videoId = videoId
			do @goFullScreen
			$(@player).width "100%"
			@active = true		

	playMedia: ->
		do @player.playVideo

	pauseMedia: ->
		do @player.pauseVideo

	stopMedia: ->
		do @player.stopVideo
		do @hidePlayer
		@videoId = ""
		do @player.clearVideo

	seekTo: (time) ->
		@player.seekTo time

	changeVolume: (volume) ->
		@player.setVolume parseInt(volume)

	goFullScreen: ->
		$(@player).height($(window).height())

	hidePlayer:->
		$(@player).height 0
		$(@player).width 0
		@active = false

	isActive: ->
		@active

	getCurrentTime: ->
		do @player.getCurrentTime 

window.YoutubeView = YoutubeView

