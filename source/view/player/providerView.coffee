class ProviderView extends Backbone.View

	preloadMedia: ->
		
	startMedia: ->

	playMedia: ->

	pauseMedia: ->

	stopMedia: ->

	seekTo: ->

	changeVolume: ->

	getCurrentTime: ->

	goFullScreen: ->